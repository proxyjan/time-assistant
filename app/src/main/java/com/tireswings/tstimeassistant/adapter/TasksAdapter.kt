package com.tireswings.tstimeassistant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.TaskEntity
import com.tireswings.tstimeassistant.views.TaskView


class TasksAdapter(private var context: Context, private var tasks: ArrayList<TaskEntity>) : BaseAdapter() {

    override fun getCount(): Int {
        return tasks.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): TaskEntity {
        return tasks[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: TaskView = (convertView ?: LayoutInflater.from(context).inflate(R.layout.task, parent, false)) as TaskView
        getItem(position).also {view.setTask(it) }
        return view
    }

    fun updateTasks(r: List<TaskEntity>) {
        this.tasks.clear()
        this.tasks.addAll(r)
    }
}



