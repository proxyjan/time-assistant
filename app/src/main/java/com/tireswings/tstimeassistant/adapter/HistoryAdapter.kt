package com.tireswings.tstimeassistant.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.database.RecordEntity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryAdapter(private var context: Context, private var records: ArrayList<RecordEntity>) : BaseAdapter() {

    override fun getCount(): Int {
        return records.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): RecordEntity {
        return records[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val record = getItem(position)
        var includeDate = false

        if(position == 0) {
            includeDate = true
        }
        else{
            if(getItem(position -1).date != record.date){
                includeDate = true
            }
        }

        val view = TextView(context)
        val entity = getItem(position)
        val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.forLanguageTag("en"))
        var dateText = "${formatter.format(entity.date)} \n"
        val t = "${(if(includeDate) dateText else "")} ${entity.taskTitle} -> ${entity.doneTime}"
        view.text = t
        return view
    }

    fun updateRecords(r: List<RecordEntity>) {
        this.records.clear()
        this.records.addAll(r)
    }
}



