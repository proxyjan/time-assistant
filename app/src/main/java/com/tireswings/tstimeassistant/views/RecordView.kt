package com.tireswings.tstimeassistant.views

import android.content.Context
import android.graphics.*
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.RecordEntity
import java.util.*
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min


@Suppress("DEPRECATION")
class RecordView: View{
    private var mTitle: String = ""
    private var mValue: Int = 0
    private var mMax: Int = 10
    private var mAccentColor: Int = Color.WHITE
    private var mRowCount: Int = 8
    private var mDate: Date = Date()
    private var mUnitSurfacePercentage: Int = 80
    private var mTextLayoutSpacingAddition: Float = 1f
    private var mTextLayoutSpacingMultiplier: Float = 0f
    private var mTextLayoutIncludePadding: Boolean = false
    private var mW: Int = width
    private var mH: Int = height
    private var innerStrokeSize: Float = 5f
    private lateinit var mTextPaint: TextPaint
    private lateinit var mBackgroundPaint: Paint
    private lateinit var mSquarePaintFrame: Paint
    private lateinit var mSquarePaint: Paint
    private lateinit var mStaticLayout: StaticLayout

    init {
        initVariables()
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet){
        context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.RecordView,
            0, 0).apply {

            try {
                mTitle = getString(R.styleable.RecordView_title) ?: "untitled"
                mValue = getInteger(R.styleable.RecordView_value, 0)
                mMax = getInteger(R.styleable.RecordView_max, 0)
                mAccentColor = getColor(R.styleable.RecordView_color, Color.WHITE)

            } finally {
                recycle()
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mW = w
        mH = h
        super.onSizeChanged(w, h, oldw, oldh)
    }

    private fun initVariables(){
        mTextPaint = TextPaint().also {
            it.isAntiAlias = true
            it.color = mAccentColor
            it.textSize = 20 * resources.displayMetrics.density
        }

        mBackgroundPaint = Paint().also {
            it.isAntiAlias = true
            it.color = mAccentColor
            it.alpha = 50
            it.style = Paint.Style.FILL
            it.shader = LinearGradient(0f,0f, width.toFloat(), height.toFloat(), Color.WHITE, mAccentColor, Shader.TileMode.CLAMP)
        }

        mSquarePaintFrame = Paint().also {
            it.isAntiAlias = true
            it.color = Color.BLACK
            it.alpha = 95
            it.style = Paint.Style.FILL_AND_STROKE
            it.strokeWidth = 5f
        }

        mSquarePaint = Paint().also {
            it.isAntiAlias = true
            it.color = mAccentColor
            it.style = Paint.Style.FILL
        }

        mStaticLayout = createStaticLayout(width)
    }

    private fun widthDependantCalculations(){
        mBackgroundPaint.shader = LinearGradient(0f,0f, width.toFloat(), height.toFloat(), Color.WHITE, mAccentColor, Shader.TileMode.CLAMP)
    }

    private fun createStaticLayout(width: Int): StaticLayout{
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val sb = StaticLayout.Builder.obtain(mTitle as CharSequence, 0, mTitle.length,  mTextPaint, width)
                .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                .setLineSpacing(mTextLayoutSpacingAddition, mTextLayoutSpacingMultiplier).setIncludePad(mTextLayoutIncludePadding)
            sb.build()
        } else {
            StaticLayout(mTitle as CharSequence, mTextPaint, width, Layout.Alignment.ALIGN_NORMAL, mTextLayoutSpacingMultiplier, mTextLayoutSpacingAddition, mTextLayoutIncludePadding
            )
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)

        val desiredWidth = 1000

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> min(desiredWidth, widthSize)
            else -> desiredWidth }

        setMeasuredDimension(width, calculateHeight(width))
    }

    fun calculateHeight(width: Int) : Int{

        mStaticLayout = createStaticLayout(width)

        val unitShare = width * mUnitSurfacePercentage / 100
        val unitSize = unitShare / mRowCount
        val unitPadding = (width - unitShare) / (mRowCount + 1)

        val widthCount = (width - unitPadding) / (unitSize + unitPadding)
        val maxCount = max(mMax, mValue)
        val heightCount = ceil(maxCount / widthCount.toDouble())

        val height = mStaticLayout.height + unitPadding + unitPadding + (heightCount * (unitSize + unitPadding))

        return height.toInt()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        widthDependantCalculations()

        val width = mW
        val height = mH

        val unitShare = width * mUnitSurfacePercentage / 100
        val unitSize = unitShare / mRowCount
        val unitPadding = (width - unitShare) / (mRowCount + 1)

        val widthCount = (width - unitPadding) / (unitSize + unitPadding)
        val heightCount = (height - unitPadding) / (unitSize + unitPadding)

        canvas.drawRect(0f,0f, width.toFloat(), height.toFloat(), mBackgroundPaint)

        var current = 0
        for (i in 1..heightCount){
            val top = (unitSize * (i - 1)) + (unitPadding * i) + mStaticLayout.height + unitPadding
            for(j in 1..widthCount){
                current++
                val max = max(mMax, mValue)
                if(current > max)
                    break

                val left = (unitSize * (j -1)) + (unitPadding * j)


                if(current <= mMax)
                    canvas.drawRoundRect(left.toFloat(), top.toFloat(), left.toFloat() + unitSize, top.toFloat() + unitSize, 15f, 15f, mSquarePaintFrame)
                if(current <= mValue)
                    canvas.drawRoundRect(left.toFloat() + innerStrokeSize, top.toFloat() + innerStrokeSize, left.toFloat() + unitSize - innerStrokeSize, top.toFloat() + unitSize - innerStrokeSize, 15f, 15f, mSquarePaint)

            }
        }

        canvas.translate(unitPadding.toFloat(), unitPadding.toFloat())
        mStaticLayout.draw(canvas)
    }

    fun setRecord(record: RecordEntity){
        mTitle = record.taskTitle
        mValue = record.doneTime.toInt()
        mMax = record.totalTime.toInt()
        mAccentColor = record.color
        mDate = record.date
        update()
    }

    fun setValue(value: Int) {
        mValue = value
        update()
    }

    private fun update(){
        initVariables()
        invalidate()
        requestLayout()
    }
}