package com.tireswings.tstimeassistant.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.motion.MotionFragment


class SettingsMotionFragment (layoutId: Int) : MotionFragment(layoutId)
{
    private lateinit var userTextView: TextView
    private lateinit var googleSignInBtn: SignInButton
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var resultLauncher: ActivityResultLauncher<Intent>


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(result.data)
            handleSignInResult(task)
        }

        super.onViewCreated(view, savedInstanceState)

        //view.findViewById<Switch>(R.id.switch_dark_mode).setOnCheckedChangeListener { _, isChecked ->
        //    if(isChecked && App.isNightModeEnabled()){
        //        App.setIsNightModeEnabled(true)
        //    }
        //}

        activity?.let { mGoogleSignInClient = GoogleSignIn.getClient(it, App.gso) }

        userTextView = view.findViewById(R.id.text_settings_user)
        googleSignInBtn = view.findViewById(R.id.sign_in_button)

        checkUserStatus()

        googleSignInBtn.setOnClickListener { signIn() }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            account?.idToken?.let {
                firebaseAuthWithGoogle(it)
                Log.w("EEEEEEEEEEEEEE", account.idToken.toString())
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("EEEEEEEEEEEEEE", "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        App.fireAuth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    Log.d("USER", "signInWithCredential:success")

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TAG", "createUserWithEmail:failure", task.exception)
                }
                checkUserStatus()
            }
    }

    private fun checkUserStatus(){
        val account = GoogleSignIn.getLastSignedInAccount(App.getAppComponent().app().applicationContext)
        if(account == null){
            Log.d("ERROR","google not signed in")
        }

        val user = App.fireAuth.currentUser

        if(user == null){
            userTextView.visibility = View.GONE
            googleSignInBtn.visibility = View.VISIBLE
        }
        else{
            userTextView.visibility = View.VISIBLE
            userTextView.text = user.displayName
            googleSignInBtn.visibility = View.GONE
        }
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        resultLauncher.launch(signInIntent)
    }
}
