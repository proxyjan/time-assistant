package com.tireswings.tstimeassistant.fragments

import android.os.Bundle
import android.view.View
import android.widget.ListView
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.adapter.HistoryAdapter
import com.tireswings.tstimeassistant.database.RecordEntity
import com.tireswings.tstimeassistant.database.Repository
import com.tireswings.tstimeassistant.motion.MotionFragment
import javax.inject.Inject

class HistoryMotionFragment (layoutId: Int) : MotionFragment(layoutId) {

    @Inject
    lateinit var rRepository: Repository
    lateinit var adapter: HistoryAdapter
    lateinit var listView: ListView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        App.getAppComponent().inject(this)
        super.onViewCreated(view, savedInstanceState)

        adapter = HistoryAdapter(App.getAppComponent().app().applicationContext, ArrayList())
        listView = view.rootView.findViewById(R.id.list_view_history)
        listView.adapter = adapter


        App.records.observe(viewLifecycleOwner, { records ->
            updateList(records.reversed())
        })

        App.fetchRecords()
    }

    private fun updateList(entities: List<RecordEntity>){
        adapter.updateRecords(entities)
        adapter.notifyDataSetChanged()
    }
}
