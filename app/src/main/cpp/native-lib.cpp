#include <stdio.h>
#include <vector>
#include "native-lib.h"

extern "C" JNIEXPORT jint JNICALL Java_com_tireswings_tstimeassistant_jni_NativeClass_sum(JNIEnv *env, jobject t, jint a, jint b) {
    return a + b;
}

extern "C" JNIEXPORT void JNICALL Java_com_tireswings_tstimeassistant_jni_NativeClass_generate(JNIEnv *env, jobject t, jintArray index, jintArray totalTime, jintArray totalDoneTime, jintArray totalPosPer, jintArray period, jintArray periodTotalTime, jintArray periodDoneTime, jintArray periodPosPer, jintArray unitDuration, jintArray remainingTime, jintArray userPriority) {
    jclass c = (*env).GetObjectClass(t);
    jmethodID callback = (*env).GetMethodID(c, "exportRecords", "([I[I)V");

    jsize s = env->GetArrayLength( index );
    std::vector<int> index_(s), totalTime_ (s), totalDoneTime_ (s), totalPosPer_ (s), period_ (s), periodTotalTime_ (s), periodDoneTime_ (s), periodPosPer_ (s), unitDuration_ (s), remainingTime_ (s), userPriority_ (s),  time_ (s), timeToBalance (s), timeToBalance_Period (s);
    jintArray result = env->NewIntArray( s );

    env->GetIntArrayRegion (index, jsize{0}, s, &index_[0]);
    env->GetIntArrayRegion (totalTime, jsize{0}, s, &totalTime_[0]);
    env->GetIntArrayRegion (totalDoneTime, jsize{0}, s, &totalDoneTime_[0]);
    env->GetIntArrayRegion (totalPosPer, jsize{0}, s, &totalPosPer_[0]);
    env->GetIntArrayRegion (period, jsize{0}, s, &period_[0]);
    env->GetIntArrayRegion (periodTotalTime, jsize{0}, s, &periodTotalTime_[0]);
    env->GetIntArrayRegion (periodDoneTime, jsize{0}, s, &periodTotalTime_[0]);
    env->GetIntArrayRegion (periodPosPer, jsize{0}, s, &periodPosPer_[0]);
    env->GetIntArrayRegion (unitDuration, jsize{0}, s, &unitDuration_[0]);
    env->GetIntArrayRegion (remainingTime, jsize{0}, s, &remainingTime_[0]);
    env->GetIntArrayRegion (userPriority, jsize{0}, s, &userPriority_[0]);


    for (unsigned int i = 0; i < s; i = i + 1)
    {
        timeToBalance[i] =  (((double)totalPosPer_[i] / 100) * totalTime_[i]) - totalDoneTime_[i];
        timeToBalance_Period[i] = (((double)periodPosPer_[i] / 100) * periodTotalTime_[i]) - periodDoneTime_[i] + remainingTime_[i];
        time_[i] = timeToBalance[i];
    }

    env->SetIntArrayRegion (result, 0, s, &time_[0]);

    (*env).CallVoidMethod(t, callback, index, result);
}