package com.tireswings.tstimeassistant.widgets

const val OPEN_RECORD_DIALOG = "com.tireswings.tstimeassistant.widgets.OPEN_RECORD_DIALOG"
const val OPEN_TASK_DIALOG = "com.tireswings.tstimeassistant.widgets.OPEN_TASK_DIALOG"
const val RECORDS_WIDGET_UPDATE = "com.tireswings.tstimeassistant.widgets.RECORDS.UPDATE"
const val TASKS_WIDGET_UPDATE = "com.tireswings.tstimeassistant.widgets.TASKS.UPDATE"