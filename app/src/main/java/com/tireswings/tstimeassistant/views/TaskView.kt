package com.tireswings.tstimeassistant.views

import android.content.Context
import android.graphics.*
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.TaskEntity
import java.lang.Float.max
import kotlin.math.max
import kotlin.math.min

@Suppress("DEPRECATION")
class TaskView: View
{
    private var mTitle: String = ""
    private var mValue: Int = 0
    private var mMax: Int = 10
    private var mAccentColor: Int = Color.WHITE
    private var mRatio: Float = 1f
    private var mW: Int = width
    private var mH: Int = height
    private var widthHeightRatio: Int = 17
    private var mTextLayoutSpacingAddition: Float = 1f
    private var mTextLayoutSpacingMultiplier: Float = 0f
    private var mTextLayoutIncludePadding: Boolean = false

    private lateinit var mTextPaint: TextPaint
    private lateinit var backgroundPaint: Paint
    private lateinit var barPaint: Paint
    private lateinit var negativePaint: Paint
    private lateinit var positivePaint: Paint
    private lateinit var mStaticLayout: StaticLayout

    init {
        initVariables()
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet){
        context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.TaskView,
            0, 0).apply {

            try {
                mTitle = getString(R.styleable.TaskView_task_title) ?: "untitled"
                mValue = getInteger(R.styleable.TaskView_task_value, 0)
                mMax = getInteger(R.styleable.TaskView_task_max, 0)
                mAccentColor = getColor(R.styleable.TaskView_task_color, Color.WHITE)

            } finally {
                recycle()
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mW = w
        mH = h
        super.onSizeChanged(w, h, oldw, oldh)
    }

    private fun initVariables(){
        mTextPaint = TextPaint().also {
            it.isAntiAlias = true
            it.color = mAccentColor
            it.textSize = 20 * resources.displayMetrics.density
        }

        backgroundPaint = Paint().also {
            it.isAntiAlias = true
            it.color = Color.WHITE
            it.alpha = 30
            it.style = Paint.Style.FILL
        }

        barPaint = Paint().also {
            it.isAntiAlias = true
            it.color = mAccentColor
            it.alpha = 90
            it.style = Paint.Style.FILL
            it.shader = LinearGradient(0f,0f, width.toFloat(), height.toFloat(), Color.WHITE, mAccentColor, Shader.TileMode.CLAMP)
        }

        negativePaint = Paint().also {
            it.isAntiAlias = true
            it.color = Color.RED
            it.alpha = 70
            it.style = Paint.Style.FILL
        }

        positivePaint = Paint().also {
            it.isAntiAlias = true
            it.color = Color.GREEN
            it.alpha = 70
            it.style = Paint.Style.FILL
        }

        mStaticLayout = createStaticLayout(width)
    }

    private fun widthDependantCalculations(){
        barPaint.shader = LinearGradient(0f,0f, width.toFloat(), height.toFloat(), mAccentColor, Color.WHITE, Shader.TileMode.CLAMP)
    }

    private fun createStaticLayout(width: Int): StaticLayout {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val sb = StaticLayout.Builder.obtain(mTitle as CharSequence, 0, mTitle.length,  mTextPaint, width)
                .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                .setLineSpacing(mTextLayoutSpacingAddition, mTextLayoutSpacingMultiplier).setIncludePad(mTextLayoutIncludePadding)
            sb.build()
        } else {
            StaticLayout(mTitle as CharSequence, mTextPaint, width, Layout.Alignment.ALIGN_NORMAL, mTextLayoutSpacingMultiplier, mTextLayoutSpacingAddition, mTextLayoutIncludePadding
            )
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)

        val desiredWidth = 1000

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> min(desiredWidth, widthSize)
            else -> desiredWidth }

        setMeasuredDimension(width, calculateHeight(width))
    }

    fun calculateHeight(width: Int) : Int{
        mStaticLayout = createStaticLayout(width)

        return max(mStaticLayout.height,  width * widthHeightRatio / 100)
    }




    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        widthDependantCalculations()

        val width = mW
        val height = mH
        mRatio = mValue.toFloat() / mMax.toFloat()

        val end = width.toFloat() * mRatio
        val mid = width.toFloat() * 0.5f

        canvas.drawRect(0f,0f, width.toFloat(), height.toFloat(), backgroundPaint)

        canvas.drawRect(0f,0f, end, height.toFloat(), barPaint)

        if(end - mid > 0){
            canvas.drawRect(mid,0f, end, height.toFloat(), positivePaint)
        }
        if(end - mid < 0){
            canvas.drawRect(end,0f, mid, height.toFloat(), negativePaint)
        }

        val hPadding = (height - mStaticLayout.height) / 2

        canvas.translate(width * 5f / 100, hPadding.toFloat())
        mStaticLayout.draw(canvas)
    }

    fun setTask(task: TaskEntity){
        mTitle = task.title
        mMax = task.unitsTotal.toInt()
        mAccentColor = task.color
        update()
    }

    fun setValue(value: Int) {
        mValue = value
        update()
    }

    private fun update(){
        initVariables()
        invalidate()
        requestLayout()
    }
}