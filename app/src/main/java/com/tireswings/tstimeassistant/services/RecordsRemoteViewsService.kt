package com.tireswings.tstimeassistant.services

import android.content.Intent
import android.widget.RemoteViewsService
import com.tireswings.tstimeassistant.adapter.RecordsRemoteViewsFactory
import kotlin.collections.ArrayList


class RecordsRemoteViewsService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return RecordsRemoteViewsFactory(this.applicationContext, ArrayList())
    }
}