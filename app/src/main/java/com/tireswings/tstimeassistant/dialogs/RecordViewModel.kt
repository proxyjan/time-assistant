package com.tireswings.tstimeassistant.dialogs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.database.RecordEntity
import java.util.*


class RecordViewModel : ViewModel() {

    private var mRecordEntity: MutableLiveData<RecordEntity> = MutableLiveData()

    fun setRecord(date: Date, taskTitle: String){
        App.getAppComponent().repository().getRecordAsync(App.mainThreadHandler, ::updateRecord, taskTitle, date)
    }

    fun updateRecord(record: RecordEntity?){
        mRecordEntity.value = record
    }

    fun getRecord(): LiveData<RecordEntity>{
        return mRecordEntity
    }
}