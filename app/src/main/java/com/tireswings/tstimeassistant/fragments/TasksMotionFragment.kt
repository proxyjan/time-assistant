package com.tireswings.tstimeassistant.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ListView
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.adapter.TasksAdapter
import com.tireswings.tstimeassistant.database.Repository
import com.tireswings.tstimeassistant.database.TaskEntity
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_DATE
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE
import com.tireswings.tstimeassistant.dialogs.TaskDialog
import com.tireswings.tstimeassistant.motion.MotionFragment
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class TasksMotionFragment (layoutId: Int) : MotionFragment(layoutId)
{
    @Inject
    lateinit var repo: Repository
    lateinit var adapter: TasksAdapter
    lateinit var listView: ListView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        App.getAppComponent().inject(this)
        super.onViewCreated(view, savedInstanceState)

        adapter = TasksAdapter(App.getAppComponent().app().applicationContext, ArrayList())
        listView = view.rootView.findViewById(R.id.list_view_tasks)
        listView.adapter = adapter

        App.tasks.observe(viewLifecycleOwner, { tasks ->
            updateList(tasks)
        })

        view.rootView.findViewById<Button>(R.id.tasks_new_button).setOnClickListener {
            val today = App.getToday()
            val deadLine = Calendar.getInstance()
            deadLine.time = today
            deadLine.add(Calendar.DATE, 10)

            val task = TaskEntity(
                "untitled", 0,1,
                today,
                today,
                Color.WHITE ,
                deadLine.time ,45, isEnabled = true, hasDeadline = false)

            repo.insertTaskAsync(App.mainThreadHandler,
                {result ->
                    print(result)

                },
                task).also {
                val intent = Intent(context, TaskDialog::class.java)
                intent.putExtra(EXTRA_ITEM_TASK_TITLE, task.title)
                startActivity(intent)

            }
        }

        App.fetchTasks()
    }

    private fun updateList(tasks: List<TaskEntity>){
        adapter.updateTasks(tasks)
        listView.setOnItemClickListener { _, _, position, _ ->
            val task = listView.getItemAtPosition(position) as TaskEntity
            val intent = Intent(context, TaskDialog::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(EXTRA_ITEM_TASK_TITLE, task.title)
            startActivity(intent)
        }
        adapter.notifyDataSetChanged()
    }
}
