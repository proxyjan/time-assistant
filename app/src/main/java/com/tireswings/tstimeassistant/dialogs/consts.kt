package com.tireswings.tstimeassistant.dialogs

const val EXTRA_ITEM_TASK_TITLE = "com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE"
const val EXTRA_ITEM_TASK_DATE = "com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_DATE"