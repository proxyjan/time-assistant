package com.tireswings.tstimeassistant.database

import androidx.room.Room
import com.tireswings.tstimeassistant.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule(private var mApp: App) {
    private val mDBName = "test_database.db"

    @Singleton
    @Provides
    fun database(): AppDatabase {
        return Room.databaseBuilder(
            mApp.applicationContext,
            AppDatabase::class.java,
            mDBName
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun recordDao(appDatabase: AppDatabase): RecordDao {
        return appDatabase.recordDao()
    }

    @Singleton
    @Provides
    fun taskDao(appDatabase: AppDatabase): TaskDao {
        return appDatabase.taskDao()
    }

    @Singleton
    @Provides
    fun repository(recordDao: RecordDao, taskDao: TaskDao): Repository {
        return Repository(recordDao, taskDao)
    }
}