package com.tireswings.tstimeassistant

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tireswings.tstimeassistant.database.*
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*


@RunWith(AndroidJUnit4::class)
class DatabaseUnitTest {
    private lateinit var recordDao: RecordDao
    private lateinit var taskDao: TaskDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        recordDao = db.recordDao()
        taskDao = db.taskDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insert_and_delete_unique_record() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        var recordOfTomorrow = recordDao.findByDate(calendar.time)
        if(recordOfTomorrow != null){
            recordDao.delete(recordOfTomorrow)
            recordOfTomorrow = recordDao.findByDate(calendar.time)
            if(recordOfTomorrow != null){
                Assert.fail("Wrong data found in database and couldn't be deleted. Something is wrong. Exiting the test!")
            }
        }
        val oldRecords = recordDao.getAll()
        val oldRecordsCount = oldRecords.count()
        val task = createTask("test")
        val newRecord = createRecord(calendar.time, "test")
        recordDao.insertAll(newRecord)
        val records = recordDao.getAll()
        val recordsCount = records.count()
        if(recordsCount != oldRecordsCount +1){
            Assert.assertTrue("Insertion didn't work. data didn't add to the previous ones.", false)
        }
        else{
            if(recordDao.findByDate(calendar.time) != null){
                recordDao.delete(newRecord)
            }
        }
        val finalRecords = recordDao.getAll()
        val finalRecordsCount = finalRecords.count()

        taskDao.delete(task)

        Assert.assertTrue("Successfully got records, inserted, find and then deleted in the table", finalRecordsCount == oldRecordsCount)
    }

    @Test
    @Throws(Exception::class)
    fun insert_and_delete_unique_task(){
        val title = "test-task"
        val newTask = createTask(title)
        if(taskDao.findByTitle(title) != null){
            taskDao.delete(newTask)
            Assert.assertTrue("False data couldn't be deleted." , taskDao.findByTitle(title) == null)
        }
        taskDao.insertAll(newTask)
        Assert.assertTrue("Data didn't inserted to the database.", taskDao.findByTitle(title) != null)
        taskDao.delete(newTask)
        Assert.assertTrue("Data couldn't be deleted!", taskDao.findByTitle(title) == null)
    }

    private fun createRecord(date: Date, task: String): RecordEntity {
        return RecordEntity(date, task, 0f,0f)
    }

    private fun createTask(title: String): TaskEntity {
        return TaskEntity(title,0f, 0f, Calendar.getInstance().time, Calendar.getInstance().time, Calendar.getInstance().time,1, true )
    }
}
