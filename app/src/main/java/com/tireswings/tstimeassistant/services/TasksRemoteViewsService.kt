package com.tireswings.tstimeassistant.services

import android.content.Intent
import android.widget.RemoteViewsService
import com.tireswings.tstimeassistant.adapter.TasksRemoteViewsFactory
import kotlin.collections.ArrayList


class TasksRemoteViewsService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return TasksRemoteViewsFactory(this.applicationContext, ArrayList())
    }
}