package com.tireswings.tstimeassistant.motion

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.drawToBitmap
import androidx.fragment.app.Fragment

open class MotionFragment(layoutId: Int) : Fragment()
{
    private var mLayoutId : Int = -1
    init {
        mLayoutId = layoutId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(mLayoutId, container, false)
    }

    fun turnIntoImage(imageView: ImageView){
        view?.let {
            imageView.setImageBitmap(it.drawToBitmap(Bitmap.Config.ARGB_8888))
        }
    }
}