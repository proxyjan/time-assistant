package com.tireswings.tstimeassistant.adapter


import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Binder
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.view.drawToBitmap
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.RecordEntity
import com.tireswings.tstimeassistant.database.TaskEntity
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_DATE
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE
import com.tireswings.tstimeassistant.views.RecordView
import com.tireswings.tstimeassistant.views.TaskView


class TasksRemoteViewsFactory(private var context: Context, private var tasks: ArrayList<TaskEntity>) : RemoteViewsService.RemoteViewsFactory {

    private lateinit var bitmap: Bitmap

    override fun onCreate() {
        bitmap = Bitmap.createBitmap(1000,250, Bitmap.Config.ARGB_8888)
    }

    override fun onDestroy() {}

    override fun getCount(): Int {
        return tasks.count()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getLoadingView(): RemoteViews {
        val remoteView = RemoteViews(context.packageName, R.layout.loading)
        remoteView.setImageViewBitmap(R.id.record_loading, bitmap)
        return remoteView
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onDataSetChanged() {
        val identityToken = Binder.clearCallingIdentity()
        Binder.restoreCallingIdentity(identityToken)
        val rs = App.getAppComponent().taskDao().getAll()
        tasks.clear()
        tasks.addAll(rs)
    }

    private fun getItem(position: Int): TaskEntity{
        return tasks[position]
    }

    override fun getViewAt(position: Int): RemoteViews {
        val remoteView = RemoteViews(context.packageName, R.layout.task_holder)
        val taskEntity = getItem(position)
        val fillInIntent = Intent().putExtras(Bundle().also {
            it.putString(EXTRA_ITEM_TASK_TITLE, tasks[position].title)
            //it.putSerializable(EXTRA_ITEM_TASK_DATE, taskEntity.createdDate)
        })
        TaskView(context).also{
            it.setTask(taskEntity)
            val height = it.calculateHeight(1000)
            it.measure(1000,height)
            it.layout(0,0, 1000,height)
            remoteView.setImageViewBitmap(R.id.task_holder, it.drawToBitmap(Bitmap.Config.ARGB_8888))
            remoteView.setOnClickFillInIntent(R.id.task_holder, fillInIntent)
        }

        return remoteView
    }
}

