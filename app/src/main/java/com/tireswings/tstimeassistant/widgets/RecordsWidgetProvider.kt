package com.tireswings.tstimeassistant.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_DATE
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE
import com.tireswings.tstimeassistant.dialogs.RecordDialog
import com.tireswings.tstimeassistant.services.RecordsRemoteViewsService
import java.util.*

class RecordsWidgetProvider : AppWidgetProvider() {

    private var recordDialogPendingIntent: PendingIntent? = null

    override fun onEnabled(context: Context?) {
        recordDialogPendingIntent = Intent(context, RecordDialog::class.java)
            .let { intent ->
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                PendingIntent.getActivity(context, 0, intent, 0)
            }
        super.onEnabled(context)
    }

    override fun onReceive(context: Context, intent: Intent) {
        val mgr: AppWidgetManager = AppWidgetManager.getInstance(context)
        //val appWidgetId: Int = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)

        if (intent.action == OPEN_RECORD_DIALOG) {
            val recordTitle: String? = intent.getStringExtra(EXTRA_ITEM_TASK_TITLE)
            val recordDate: Date = intent.getSerializableExtra(EXTRA_ITEM_TASK_DATE) as Date

            val i = Intent(context, RecordDialog::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
            i.putExtra(EXTRA_ITEM_TASK_DATE, recordDate)
            i.putExtra(EXTRA_ITEM_TASK_TITLE, recordTitle)
            context.startActivity(i)
        } // On Click for each record

        if (intent.hasExtra(RECORDS_WIDGET_UPDATE)) {
           val ids = intent.extras!!.getIntArray(RECORDS_WIDGET_UPDATE)
           onUpdate(context, mgr, ids!!)
        }
        super.onReceive(context, intent)
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        appWidgetIds.forEach { appWidgetId ->
            val views = RemoteViews(context.packageName, R.layout.records_layout)
            val intent = Intent(context, RecordsRemoteViewsService::class.java).apply {
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
            }
            views.setRemoteAdapter(R.id.list_view_records, intent)

            val nowWidgetPendingIntent: PendingIntent = Intent(
                context, RecordsWidgetProvider::class.java
            ).run {
                action = OPEN_RECORD_DIALOG
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
                PendingIntent.getBroadcast(context, 0, this, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            views.setPendingIntentTemplate(R.id.list_view_records, nowWidgetPendingIntent)

            appWidgetManager.updateAppWidget(appWidgetId, views)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.list_view_records)
        }
    }

    companion object {
        fun updateWidget(context: Context) {
            val man = AppWidgetManager.getInstance(context)
            val ids = man.getAppWidgetIds(
                ComponentName(context, RecordsWidgetProvider::class.java)
            )
            val updateIntent = Intent()
            updateIntent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            updateIntent.putExtra(RECORDS_WIDGET_UPDATE, ids)
            context.sendBroadcast(updateIntent)
        }
    }
}