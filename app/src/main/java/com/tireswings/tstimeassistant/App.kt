package com.tireswings.tstimeassistant

import android.R
import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.core.os.HandlerCompat
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.tireswings.tstimeassistant.database.*
import com.tireswings.tstimeassistant.jni.NativeClass
import com.tireswings.tstimeassistant.widgets.RecordsWidgetProvider
import com.tireswings.tstimeassistant.widgets.TasksWidgetProvider
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class App : Application() {

    private val component: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .databaseModule(DatabaseModule(this))
            .build()
    }

    companion object {
        private var isNightMode = true
        private lateinit var appComponent: AppComponent

        private lateinit var fireStorage: StorageReference
        lateinit var fireAuth: FirebaseAuth
        lateinit var fireDatabase: FirebaseDatabase
        val executorService: ExecutorService = Executors.newFixedThreadPool(4)
        val mainThreadHandler: Handler = HandlerCompat.createAsync(Looper.getMainLooper())
        val records: MutableLiveData<ArrayList<RecordEntity>> = MutableLiveData<ArrayList<RecordEntity>>(ArrayList())
        val nowRecords: MutableLiveData<ArrayList<RecordEntity>> = MutableLiveData<ArrayList<RecordEntity>>(ArrayList())
        val tasks: MutableLiveData<ArrayList<TaskEntity>> = MutableLiveData<ArrayList<TaskEntity>>(ArrayList())
        var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("809704153098-8072410c5p55ffs04vmuo15346m619vk.apps.googleusercontent.com")
            .build()

        fun setupFirebase(){
            FirebaseApp.initializeApp(appComponent.app().applicationContext)
            fireAuth = FirebaseAuth.getInstance()
            fireDatabase = FirebaseDatabase.getInstance()
            fireStorage = FirebaseStorage.getInstance().reference

        }

        private fun removeTime(date: Date): Date {
            val cal: Calendar = Calendar.getInstance()
            cal.time = date
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.SECOND, 0)
            cal.set(Calendar.MILLISECOND, 0)
            return cal.time
        }

        fun getToday(): Date{
            return removeTime(Date())
        }

        fun fetchRecords(){
            getAppComponent().repository().getRecordsAsync(mainThreadHandler, ::updateRecords)
        }

        fun fetchTasks(){
            getAppComponent().repository().getTasksAsync(mainThreadHandler, ::updateTasks)
        }

        private fun updateRecords(recordEntities: List<RecordEntity>){
            records.value = ArrayList(recordEntities)
            val taskTitles = tasks.value?.map{t -> t.title}!!
            nowRecords.value = ArrayList(recordEntities.filter { r -> r.taskTitle in taskTitles && r.date == getToday() })
            RecordsWidgetProvider.updateWidget(appComponent.app().applicationContext)
        }

        private fun updateTasks(taskEntities: List<TaskEntity>){
            tasks.value = ArrayList(taskEntities)
            TasksWidgetProvider.updateWidget(appComponent.app().applicationContext)
        }

        fun setAppComponent(a: AppComponent) {
            appComponent = a
        }

        fun getAppComponent(): AppComponent{
            return appComponent
        }

        fun isNightModeEnabled() : Boolean {
            return isNightMode
        }

        fun setIsNightModeEnabled(isEnabled: Boolean) {
            if(isNightMode!= isEnabled){
                isNightMode = isEnabled

                PreferenceManager.getDefaultSharedPreferences(appComponent.app().applicationContext).edit().putBoolean(
                    "NIGHT_MODE",
                    isNightMode
                ).apply()
            }
        }
    }

    override fun onCreate() {
        val mPrefs =  PreferenceManager.getDefaultSharedPreferences(this)
        isNightMode = mPrefs.getBoolean("NIGHT_MODE", false)

        super.onCreate()
        component.inject(this)
        setAppComponent(component)
        setupFirebase()
    }
}