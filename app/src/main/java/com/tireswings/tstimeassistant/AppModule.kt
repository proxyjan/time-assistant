package com.tireswings.tstimeassistant

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private var mApp: App) {
    @Provides
    @Singleton
    fun providesApp(): Application {
        return mApp
    }
}