package com.tireswings.tstimeassistant.database

import androidx.room.Embedded
import androidx.room.Relation

data class TaskWithRecords(
    @Embedded val task: TaskEntity,
    @Relation(parentColumn = "title", entityColumn = "taskTitle")
    val records: List<RecordEntity>
)