package com.tireswings.tstimeassistant

import android.app.Application
import com.tireswings.tstimeassistant.activities.MainActivity
import com.tireswings.tstimeassistant.database.*
import com.tireswings.tstimeassistant.fragments.HistoryMotionFragment
import com.tireswings.tstimeassistant.fragments.NowMotionFragment
import com.tireswings.tstimeassistant.fragments.TasksMotionFragment
import com.tireswings.tstimeassistant.jni.NativeClass
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DatabaseModule::class, AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(app: App)
    fun inject(nowMotionFragment: NowMotionFragment)
    fun inject(taskMotionFragment: TasksMotionFragment)
    fun inject(historyMotionFragment: HistoryMotionFragment)
    fun inject(nativeClass: NativeClass)
    fun app(): Application
    fun recordDao(): RecordDao
    fun taskDao(): TaskDao
    fun appDatabase(): AppDatabase
    fun repository(): Repository
}