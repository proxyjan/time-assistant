package com.tireswings.tstimeassistant.database

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.*

@Entity(tableName = "tasks")
data class TaskEntity(
    @PrimaryKey var title: String,
    var unitsTotal: Int,
    var unitsPeriod: Int,
    var createdDate: Date,
    var modifiedDate: Date,
    var color: Int,
    var deadlineDate: Date,
    var unitDuration: Int,
    var isEnabled: Boolean,
    var hasDeadline: Boolean)


