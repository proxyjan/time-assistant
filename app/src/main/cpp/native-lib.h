#include <jni.h>
#ifndef _Included_NativeSample
#define _Included_NativeSample
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_com_tireswings_tstimeassistant_jni_NativeClass_sum(JNIEnv *, jobject, jint, jint);


JNIEXPORT void JNICALL Java_com_tireswings_tstimeassistant_jni_NativeClass_generate(JNIEnv *, jobject, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray, jintArray);

#ifdef __cplusplus
}
#endif
#endif