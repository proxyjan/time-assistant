package com.tireswings.tstimeassistant.dialogs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.database.TaskEntity


class TaskViewModel : ViewModel() {

    private var taskEntity: MutableLiveData<TaskEntity> = MutableLiveData()

    var keyTitle: String? = null

    fun setTask(taskTitle: String){
        App.getAppComponent().repository().getTaskAsync(App.mainThreadHandler, ::updateTask, taskTitle)
    }

    fun updateTask(task: TaskEntity?){
        taskEntity.value = task!!
        if(keyTitle != null){
            keyTitle = task.title
        }
    }

    fun getTask(): LiveData<TaskEntity>{
        return taskEntity
    }
}