package com.tireswings.tstimeassistant.fragments

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.tireswings.tstimeassistant.dialogs.RecordDialog
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.motion.MotionFragment
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.adapter.RecordsAdapter
import com.tireswings.tstimeassistant.database.RecordEntity
import com.tireswings.tstimeassistant.database.Repository
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_DATE
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE
import com.tireswings.tstimeassistant.jni.NativeClass
import kotlinx.coroutines.flow.emptyFlow
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask


class NowMotionFragment(layoutId: Int) : MotionFragment(layoutId){

    @Inject lateinit var rRepository: Repository
    lateinit var adapter: RecordsAdapter
    lateinit var listView: ListView

    private fun onClickAction() {
        val toast = Toast.makeText(context, "Pew!", Toast.LENGTH_SHORT)
        toast.show()

        val pendingIntent: PendingIntent = Intent(context, RecordDialog::class.java)
            .let { intent ->
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                PendingIntent.getActivity(context, 0, intent, 0)
            }

        val builder = NotificationCompat.Builder(App.getAppComponent().app().applicationContext, "123")
            .setSmallIcon(R.drawable.ic_tasks)
            .setContentTitle("Ali")
            .setContentText("Ali")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText("Much longer text that cannot fit one line..."))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .addAction(R.drawable.records_widget_preview, "ee", pendingIntent)

        with(NotificationManagerCompat.from(App.getAppComponent().app().applicationContext)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, builder.build())
        }
    }

    private fun taskHandler(n: Int){
        Log.d("E", n.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.findViewById<Button>(R.id.refresh_records).setOnClickListener {
            rRepository.preProcessingDataForGeneratingRecordsAsync(App.mainThreadHandler) { GenerateInputData ->
                NativeClass().generate(
                    GenerateInputData.index,
                    GenerateInputData.totalTime,
                    GenerateInputData.totalDoneTime,
                    GenerateInputData.totalPosPer,
                    GenerateInputData.period,
                    GenerateInputData.periodTotalTime,
                    GenerateInputData.periodDoneTime,
                    GenerateInputData.periodPosPer,
                    GenerateInputData.unitDuration,
                    GenerateInputData.remainingTime,
                    GenerateInputData.userPriority
                )
            }
        }

        App.getAppComponent().inject(this)
        super.onViewCreated(view, savedInstanceState)

        adapter = RecordsAdapter(App.getAppComponent().app().applicationContext, ArrayList())
        listView = view.rootView.findViewById(R.id.list_view_records)
        listView.adapter = adapter

        App.nowRecords.observe(viewLifecycleOwner, { records ->
            updateList(records)
        })

        App.fetchRecords()
    }

    private fun updateList(entities: List<RecordEntity>){
        adapter.updateRecords(entities)
        listView.setOnItemClickListener { _, _, position, _ ->
            val record = listView.getItemAtPosition(position) as RecordEntity
            val intent = Intent(context, RecordDialog::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(EXTRA_ITEM_TASK_DATE, record.date)
            intent.putExtra(EXTRA_ITEM_TASK_TITLE, record.taskTitle)
            startActivity(intent)
        }
        adapter.notifyDataSetChanged()
    }
}