package com.tireswings.tstimeassistant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.RecordEntity
import com.tireswings.tstimeassistant.views.RecordView


class RecordsAdapter(private var context: Context, private var records: ArrayList<RecordEntity>) : BaseAdapter() {

    override fun getCount(): Int {
        return records.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): RecordEntity {
        return records[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: RecordView = (convertView ?: LayoutInflater.from(context).inflate(R.layout.record, parent, false)) as RecordView
        getItem(position).also {view.setRecord(it) }
        return view
    }

    fun updateRecords(r: List<RecordEntity>) {
        this.records.clear()
        this.records.addAll(r)
    }
}



