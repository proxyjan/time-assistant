package com.tireswings.tstimeassistant.dialogs

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.TaskEntity
import java.util.*
import android.text.Editable

import android.text.TextWatcher
import android.util.Log


class TaskDialog : AppCompatActivity()
{
    private val c: Calendar = Calendar.getInstance()
    private var year = c.get(Calendar.YEAR)
    private var month = c.get(Calendar.MONTH)
    private var day = c.get(Calendar.DAY_OF_MONTH)

    private lateinit var confirmButton: Button
    private lateinit var deadLineCheckBox: CheckBox
    private lateinit var deadLineText: TextView
    private lateinit var taskTitleEditText: EditText
    private lateinit var deleteTextView: TextView
    private lateinit var unitCount: NumberPicker
    private lateinit var period: NumberPicker
    private lateinit var unitDuration: EditText

    private val periodMapperUItoData = mapOf(0 to 1, 1 to 7, 2 to 30, 3 to 360)
    private val periodMapperDataToUI = mapOf(1 to 0, 7 to 1, 30 to 2, 360 to 3)

    private val model: TaskViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        if (!App.isNightModeEnabled()) setTheme(R.style.ActivityTheme_Primary_Base_Dark)
        else setTheme(R.style.ActivityTheme_Primary_Base_Light)

        val taskTitle = intent.getSerializableExtra(EXTRA_ITEM_TASK_TITLE) as String

        model.getTask().observe(this, { task ->
            updateView(task)
        })
        model.setTask(taskTitle)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.task_dialog)

        unitCount = findViewById(R.id.task_unit_number_picker)
        unitCount.minValue = 1
        unitCount.maxValue = 100

        period = findViewById(R.id.task_period_picker)
        period.minValue = 0
        period.maxValue = 3
        period.displayedValues = arrayOf("day", "week", "month", "year")

        taskTitleEditText = findViewById(R.id.task_dialog_title_text)
        confirmButton = findViewById(R.id.confirm_changes)
        deadLineCheckBox = findViewById(R.id.hasDeadline)
        deadLineText = findViewById(R.id.deadlineText)
        deleteTextView = findViewById(R.id.task_dialog_delete_text)
        unitDuration = findViewById(R.id.task_unit_duration)

        taskTitleEditText.visibility = View.INVISIBLE
        deadLineText.visibility = View.INVISIBLE
        unitDuration.visibility = View.INVISIBLE


        deadLineText.setOnClickListener{openDatePicker()}

        deleteTextView.setOnClickListener {
            model.getTask().value?.let { task ->
                App.getAppComponent().repository().deleteTaskAsync(
                    App.mainThreadHandler,
                    { result -> print(result); App.fetchTasks(); finish()},
                    task
                )
            }
        }

        taskTitleEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.isNotEmpty())
                    model.getTask().value?.let{ task ->
                        task.title = s.toString()
                    }
            }
        })

        unitDuration.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.isNotEmpty())
                    model.getTask().value?.let{ task ->
                        task.unitDuration = s.toString().toInt()
                    }
            }
        })


        unitCount.setOnValueChangedListener { _, _, newVal -> updateCountPicker(newVal) }
        period.setOnValueChangedListener { _, _, newVal -> updatePeriodPicker(newVal) }
        deadLineCheckBox.setOnCheckedChangeListener { _, b -> updateDeadlineCheckBox(b)}

        findViewById<Button>(R.id.colors_blue).setOnClickListener { updateColor(Color.BLUE) }
        findViewById<Button>(R.id.colors_yellow).setOnClickListener { updateColor(Color.YELLOW) }
        findViewById<Button>(R.id.colors_cyan).setOnClickListener { updateColor(Color.CYAN) }
        findViewById<Button>(R.id.colors_red).setOnClickListener { updateColor(Color.RED) }
        findViewById<Button>(R.id.colors_green).setOnClickListener { updateColor(Color.GREEN) }
        findViewById<Button>(R.id.colors_purple).setOnClickListener { updateColor(Color.MAGENTA) }

        findViewById<Button>(R.id.confirm_changes).setOnClickListener {button ->
            model.getTask().value?.let { task ->
                val today = App.getToday()
                button.alpha = 0f
                task.modifiedDate = today
                if(task.title == model.keyTitle) {
                    App.getAppComponent().repository().updateTaskAsync(
                        App.mainThreadHandler,
                        { result ->
                            print(result); App.fetchTasks(); finish()
                            button.alpha = 1f
                        },
                        task
                    )
                }
                else{
                    App.getAppComponent().repository().deleteTaskAsync(
                        App.mainThreadHandler,
                        {
                            App.getAppComponent().repository().insertTaskAsync(
                                App.mainThreadHandler,
                                { result ->
                                    print(result); App.fetchTasks(); finish()
                                    button.alpha = 1f
                                },
                                task
                            )
                        },
                        task
                    )
                }
            }
        }
    }

    private fun updateDeadlineCheckBox(value: Boolean) {
        model.getTask().value?.let{ task ->
            task.hasDeadline = value
            model.updateTask(task)
        }
    }

    private fun updateCountPicker(value: Int){
        model.getTask().value?.let{ task ->
            task.unitsTotal = value
            model.updateTask(task)
        }
    }

    private fun updatePeriodPicker(value: Int){
        model.getTask().value?.let{ task ->
            task.unitsPeriod = periodMapperUItoData[value]!!
            model.updateTask(task)
        }
    }

    private fun updateColor(color: Int){
        model.getTask().value?.let{ task ->
            task.color = color
            model.updateTask(task)
        }
    }

    private fun updateView(t: TaskEntity){
        taskTitleEditText.visibility = View.VISIBLE
        deadLineText.visibility = View.VISIBLE
        unitDuration.visibility = View.VISIBLE

        deadLineText.text = t.deadlineDate.toString()
        taskTitleEditText.setText(t.title)
        taskTitleEditText.setTextColor(t.color)
        unitDuration.setText(t.unitDuration.toString())
        unitCount.value = t.unitsTotal
        deadLineCheckBox.isChecked = t.hasDeadline
        period.value = periodMapperDataToUI[t.unitsPeriod]!!

    }

    private fun openDatePicker(){
        val dpd = DatePickerDialog(this, {_, y, m, d ->
            c.set(y, m, d, 0, 0)

            year = y
            month = m
            day = d

            model.getTask().value?.let{ task ->
                task.deadlineDate.time = c.timeInMillis
                model.updateTask(task)
            }
        }, year, month, day)
        dpd.show()
    }
}