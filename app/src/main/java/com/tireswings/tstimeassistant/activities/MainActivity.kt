package com.tireswings.tstimeassistant.activities

import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.database.Repository
import com.tireswings.tstimeassistant.fragments.HistoryMotionFragment
import com.tireswings.tstimeassistant.fragments.NowMotionFragment
import com.tireswings.tstimeassistant.fragments.SettingsMotionFragment
import com.tireswings.tstimeassistant.fragments.TasksMotionFragment
import com.tireswings.tstimeassistant.motion.MotionFragment
import javax.inject.Inject


class MainActivity : AppCompatActivity(){

    @Inject lateinit var rRepository: Repository

    companion object {
        var instance: MainActivity? = null
        var nowFragment: Fragment? = null
        var tasksFragment: Fragment? = null
        var historyFragment: Fragment? = null
        var settingsFragment: Fragment? = null
    }


    fun hideFragment(fragment: Fragment, hostImageViewId: Int){
        if(!fragment.isHidden) {
            val imageView: ImageView = findViewById(hostImageViewId)
            (fragment as MotionFragment).turnIntoImage(imageView)
            supportFragmentManager.beginTransaction()
                .hide(fragment)
                .commitNow()
        }
    }

    fun showFragment(fragment: Fragment, hostImageViewId: Int){
        if(fragment.isHidden) {
            val imageView: ImageView = findViewById(hostImageViewId)
            supportFragmentManager.beginTransaction()
                .show(fragment)
                .commitNow()
            imageView.setImageResource(0)
        }
    }

    private fun setupFragments(){
        val transaction = supportFragmentManager.beginTransaction()

        val fragments = arrayOf(
            tasksFragment,
            nowFragment,
            historyFragment,
            settingsFragment
        )
        val placeHolders = arrayOf(
            R.id.tasks_placeholder,
            R.id.now_placeholder,
            R.id.history_placeholder,
            R.id.settings_placeholder
        )

        for ((fragment, placeHolder) in fragments.zip(placeHolders)){
            fragment?.let {
                if(it.isDetached)
                    transaction.attach(it)
                else
                    transaction.replace(placeHolder, it)

                if(it !is NowMotionFragment)
                    transaction.hide(it)
            }
        }

        transaction.commitNow()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        if (!App.isNightModeEnabled()) setTheme(R.style.ActivityTheme_Primary_Base_Dark)
        else setTheme(R.style.ActivityTheme_Primary_Base_Light)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        instance = this
        tasksFragment = TasksMotionFragment(R.layout.tasks_layout)
        nowFragment = NowMotionFragment(R.layout.records_layout)
        historyFragment = HistoryMotionFragment(R.layout.history_layout)
        settingsFragment = SettingsMotionFragment(R.layout.settings_layout)

        //Log.d("1 + 2: ", NativeClass().add(1,2).toString())

        setupFragments()
    }
}
