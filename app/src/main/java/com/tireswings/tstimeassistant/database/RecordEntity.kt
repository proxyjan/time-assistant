package com.tireswings.tstimeassistant.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "records", primaryKeys = ["date", "taskTitle"])
data class RecordEntity(
    var date: Date,
    var taskTitle: String,
    var color: Int,
    var doneTime: Int,
    var totalTime: Int)
