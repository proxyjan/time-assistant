package com.tireswings.tstimeassistant.dialogs

import android.os.Bundle
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.views.RecordView
import java.util.*

class RecordDialog : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        if (!App.isNightModeEnabled()) setTheme(R.style.ActivityTheme_Primary_Base_Dark)
        else setTheme(R.style.ActivityTheme_Primary_Base_Light)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.record_dialog)

        val taskTitle = intent.getSerializableExtra(EXTRA_ITEM_TASK_TITLE) as String
        val date = intent.getSerializableExtra(EXTRA_ITEM_TASK_DATE) as Date

        val recordView = findViewById<RecordView>(R.id.record_view)
        val model: RecordViewModel by viewModels()
        model.setRecord(date, taskTitle)
        model.getRecord().observe(this, { record ->
            recordView.setRecord(record)
        })

        findViewById<TextView>(R.id.record_dialog_dismiss_text).setOnClickListener{
            model.getRecord().value?.let { record ->
                App.getAppComponent().repository().deleteRecordAsync(
                    App.mainThreadHandler,
                    { v -> print(v); App.fetchRecords(); finish()
                    },
                    record)
            }
        }

        findViewById<Button>(R.id.btn_add).setOnClickListener {
            model.getRecord().value?.let{ record ->
                record.doneTime++
                model.updateRecord(record)
            }
        }

        findViewById<Button>(R.id.btn_remove).setOnClickListener {
            model.getRecord().value?.let{ record ->
                if(record.doneTime > 0)
                    record.doneTime--
                    model.updateRecord(record)
            }
        }

        findViewById<Button>(R.id.btn_confirm).setOnClickListener {button ->
            model.getRecord().value?.let {
                button.alpha = 0f
                App.getAppComponent().repository().updateRecordAsync(
                    App.mainThreadHandler,
                    { v -> print(v); App.fetchRecords(); finish()
                        button.alpha = 1f},
                    it
                )
            }
        }
    }
}