package com.tireswings.tstimeassistant.jni

import android.os.Handler
import android.util.Log
import com.tireswings.tstimeassistant.App
import com.tireswings.tstimeassistant.AppComponent
import com.tireswings.tstimeassistant.database.Repository
import com.tireswings.tstimeassistant.database.TaskEntity
import javax.inject.Inject

class NativeClass {
    companion object {
        init {
            System.loadLibrary("native-lib")
        }

        fun getSumAsync(resultHandler: Handler, callback: (Int) -> Any, a: Int, b: Int){
            App.executorService.execute {
                try {
                    val response = NativeClass().sum(a,b)
                    resultHandler.post { callback(response) }
                } catch (e: Exception) {
                    resultHandler.post { print("error getting sum result from native library...")}
                }
            }
        }
    }

    fun exportRecords(index: IntArray, time: IntArray){
        App.getAppComponent().repository().postProcessingDataForGeneratingRecordsAsync(index, time)
    }

    external fun sum(a: Int, b: Int): Int

    external fun generate(index: IntArray, totalTime: IntArray, totalDoneTime: IntArray, totalPosPer: IntArray, period: IntArray, periodTotalTime: IntArray, periodDoneTime: IntArray, periodPosPer: IntArray, unitDuration: IntArray, remainingTime: IntArray, userPriority: IntArray)
}