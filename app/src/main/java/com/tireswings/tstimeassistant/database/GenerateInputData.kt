package com.tireswings.tstimeassistant.database



data class GenerateInputData(
    var index: IntArray,
    var totalTime: IntArray,
    var totalDoneTime: IntArray,
    var totalPosPer: IntArray,
    var period: IntArray,
    var periodTotalTime: IntArray,
    var periodDoneTime: IntArray,
    var periodPosPer: IntArray,
    var unitDuration: IntArray,
    var remainingTime: IntArray,
    var userPriority: IntArray
    ) {

    constructor(count: Int) : this(
        index = IntArray(count),
        totalTime = IntArray(count),
        totalDoneTime = IntArray(count),
        totalPosPer = IntArray(count),
        period = IntArray(count),
        periodTotalTime = IntArray(count),
        periodDoneTime = IntArray(count),
        periodPosPer = IntArray(count),
        unitDuration = IntArray(count),
        remainingTime = IntArray(count),
        userPriority = IntArray(count),
    )


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GenerateInputData

        if (!index.contentEquals(other.index)) return false
        if (!totalTime.contentEquals(other.totalTime)) return false
        if (!totalDoneTime.contentEquals(other.totalDoneTime)) return false
        if (!totalPosPer.contentEquals(other.totalPosPer)) return false
        if (!period.contentEquals(other.period)) return false
        if (!periodTotalTime.contentEquals(other.periodTotalTime)) return false
        if (!periodDoneTime.contentEquals(other.periodDoneTime)) return false
        if (!periodPosPer.contentEquals(other.periodPosPer)) return false
        if (!unitDuration.contentEquals(other.unitDuration)) return false
        if (!remainingTime.contentEquals(other.remainingTime)) return false
        if (!userPriority.contentEquals(other.userPriority)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = index.contentHashCode()
        result = 31 * result + totalTime.contentHashCode()
        result = 31 * result + totalDoneTime.contentHashCode()
        result = 31 * result + totalPosPer.contentHashCode()
        result = 31 * result + period.contentHashCode()
        result = 31 * result + periodTotalTime.contentHashCode()
        result = 31 * result + periodDoneTime.contentHashCode()
        result = 31 * result + periodPosPer.contentHashCode()
        result = 31 * result + unitDuration.contentHashCode()
        result = 31 * result + remainingTime.contentHashCode()
        result = 31 * result + userPriority.contentHashCode()
        return result
    }
}

