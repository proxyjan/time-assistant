package com.tireswings.tstimeassistant.motion

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.MotionScene
import com.tireswings.tstimeassistant.activities.MainActivity
import com.tireswings.tstimeassistant.R
import kotlin.math.abs


class CustomMotionLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    private val touchSlop: Int = ViewConfiguration.get(context).scaledTouchSlop
) : MotionLayout(context, attrs) {

    private var mPreviousX: Int = -1
    private var mPreviousY: Int = -1
    private var mIsScrolling: Boolean = false
    private var mIsActive: Boolean = false
    private var mStartId: Int = -1
    private var mEndId: Int = -1
    private var mLastTransitionId: Int = -1
    private var mIsTouchDown: Boolean = false

    init{
        setTransitionListener(object : TransitionListener {
            override fun onTransitionChange(motionLayout: MotionLayout, startId: Int, endId: Int, progress: Float) {}

            override fun onTransitionTrigger(motionLayout: MotionLayout, triggerId: Int, positive: Boolean, progress: Float) {}

            override fun onTransitionStarted(motionLayout: MotionLayout, startId: Int, endId: Int) {}

            override fun onTransitionCompleted(motionLayout: MotionLayout, currentId: Int) {
                if(!mIsTouchDown)
                    onTransitionFinishedHandler(currentId)
            }
        })
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        val result = when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mPreviousX = event.x.toInt()
                mPreviousY = event.y.toInt()
                mIsScrolling = false
                false
            }
            MotionEvent.ACTION_MOVE -> {
                if (mIsScrolling)
                    true
                else {
                    val xMotion: Int = abs(mPreviousX - event.x.toInt())
                    val yMotion: Int = abs(mPreviousY - event.y.toInt())
                    mPreviousX = event.x.toInt()
                    mPreviousY = event.y.toInt()
                    if (xMotion > yMotion && yMotion < touchSlop ) {
                        mIsScrolling = true
                        true
                    }
                    else { false }
                }
            }
            else -> {
                mIsScrolling = false
                false
            }
        }

        if(!super.onInterceptTouchEvent(event) && event.action != MotionEvent.ACTION_MOVE) {
            onTouchEvent(event)
        }

        return result
    }

    override fun performClick(): Boolean {
        if (progress == 0f || progress == 1f) {
            when (mLastTransitionId) {
                R.id.now_to_tasks -> {
                    if (progress == 0f) {
                        progress = 0.001f
                        transitionToState(R.id.now_state)
                    } else {
                        progress = 0.999f
                        transitionToState(R.id.tasks_state)
                    }
                }
                R.id.now_to_history -> {
                    if (progress == 0f) {
                        progress = 0.001f
                        transitionToState(R.id.now_state)
                    } else {
                        progress = 0.999f
                        transitionToState(R.id.history_state)
                    }
                }
                R.id.history_to_settings -> {
                    if (progress == 0f) {
                        progress = 0.001f
                        transitionToState(R.id.history_state)
                    } else {
                        progress = 0.999f
                        transitionToState(R.id.settings_state)
                    }
                }
                R.id.tasks_to_tasks_end -> {
                    if (progress == 0f) {
                        progress = 0.001f
                        transitionToState(R.id.tasks_state)
                    } else {
                        progress = 0.999f
                        transitionToState(R.id.tasks_state)
                    }
                }
                R.id.settings_to_settings_end -> {
                    if (progress == 0f) {
                        progress = 0.001f
                        transitionToState(R.id.settings_state)
                    } else {
                        progress = 0.999f
                        transitionToState(R.id.settings_state)
                    }
                }
            }
        }
        return super.performClick()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if(event.action == MotionEvent.ACTION_MOVE && mIsActive)
            mIsTouchDown = true
        if(event.action == MotionEvent.ACTION_UP && mIsActive) {
            mIsTouchDown = false
            performClick()
        }

        return super.onTouchEvent(event)
    }

    private fun onTransitionFinishedHandler(stateId: Int){
        mIsActive = false
        when(stateId){
            R.id.tasks_state -> MainActivity.tasksFragment?.let{
                MainActivity.instance?.showFragment(it,
                    R.id.tasks_fragment_background_image
                )}
            R.id.now_state -> MainActivity.nowFragment?.let{
                MainActivity.instance?.showFragment(it,
                    R.id.now_fragment_background_image
                )}
            R.id.history_state -> MainActivity.historyFragment?.let{
                MainActivity.instance?.showFragment(it,
                    R.id.history_fragment_background_image
                )}
            R.id.settings_state -> MainActivity.settingsFragment?.let{
                MainActivity.instance?.showFragment(it,
                    R.id.settings_fragment_background_image
                )}
        }
    }

    private fun getTransitionId(startId: Int, endId: Int) : Int{
        var result = -1

        if(startId == R.id.now_state && endId == R.id.tasks_state) result =
            R.id.now_to_tasks
        else if(startId == R.id.now_state && endId == R.id.history_state) result =
            R.id.now_to_history
        else if(startId == R.id.history_state && endId == R.id.settings_state) result =
            R.id.history_to_settings
        else if(startId == R.id.tasks_state && endId == R.id.tasks_end_state) result =
            R.id.tasks_to_tasks_end
        else if(startId == R.id.settings_state && endId == R.id.settings_end_state) result =
            R.id.settings_to_settings_end

        return result
    }

    override fun setTransition(transition: MotionScene.Transition) {
        mStartId = transition.startConstraintSetId
        mEndId   = transition.endConstraintSetId
        val transitionId = getTransitionId(mStartId, mEndId)

        if(!mIsActive) {
            when(transitionId){
                R.id.now_to_tasks -> {
                    MainActivity.nowFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.now_fragment_background_image
                        )}
                    MainActivity.tasksFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.tasks_fragment_background_image
                        )}
                }
                R.id.now_to_history -> {
                    MainActivity.nowFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.now_fragment_background_image
                        )}
                    MainActivity.historyFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.history_fragment_background_image
                        )}
                }
                R.id.history_to_settings -> {
                    MainActivity.historyFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.history_fragment_background_image
                        )}
                    MainActivity.settingsFragment?.let{
                        MainActivity.instance?.hideFragment(it,
                            R.id.settings_fragment_background_image
                        )}
                }
                R.id.tasks_to_tasks_end -> MainActivity.tasksFragment?.let{
                    MainActivity.instance?.hideFragment(it,
                        R.id.tasks_fragment_background_image
                    )}
                R.id.settings_to_settings_end -> MainActivity.settingsFragment?.let{
                    MainActivity.instance?.hideFragment(it,
                        R.id.settings_fragment_background_image
                    )}
            }
            mLastTransitionId = transitionId
            mIsActive = true
            super.setTransition(transition)
        }
        else if(transitionId == mLastTransitionId)
            super.setTransition(transition)
    }
}