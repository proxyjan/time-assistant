package com.tireswings.tstimeassistant.database

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface RecordDao {
    @Query("SELECT * FROM records")
    fun getAll(): List<RecordEntity>

    @Query("SELECT * FROM records WHERE date IN (:recordDates)")
    fun loadAllByDates(recordDates: LongArray): LiveData<List<RecordEntity>>

    @Query("SELECT * FROM records WHERE date IS (:date)")
    fun findByDate(date: Date): LiveData<List<RecordEntity>>

    @Query("SELECT * FROM records WHERE taskTitle IS (:taskTitle)")
    fun findByTitle(taskTitle: String): LiveData<List<RecordEntity>>

    @Query("SELECT * FROM records WHERE taskTitle IS (:taskTitle) AND date IS (:date)")
    fun findUniqueRecord(taskTitle: String, date: Date): RecordEntity?

    @Update
    fun updateRecords(vararg records: RecordEntity)

    @Insert
    fun insertAll(vararg records: RecordEntity): List<Long>

    @Delete
    fun delete(record: RecordEntity) : Int
}