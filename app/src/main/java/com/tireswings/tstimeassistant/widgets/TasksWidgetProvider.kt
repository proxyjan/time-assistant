package com.tireswings.tstimeassistant.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import com.tireswings.tstimeassistant.R
import com.tireswings.tstimeassistant.dialogs.EXTRA_ITEM_TASK_TITLE
import com.tireswings.tstimeassistant.dialogs.TaskDialog
import com.tireswings.tstimeassistant.services.TasksRemoteViewsService

class TasksWidgetProvider : AppWidgetProvider() {

    private var taskDialogPendingIntent: PendingIntent? = null

    override fun onEnabled(context: Context?) {
        taskDialogPendingIntent = Intent(context, TaskDialog::class.java)
            .let { intent ->
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                PendingIntent.getActivity(context, 1, intent, 0)
            }
        super.onEnabled(context)
    }

    override fun onReceive(context: Context, intent: Intent) {
        val mgr: AppWidgetManager = AppWidgetManager.getInstance(context)
        //val appWidgetId: Int = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)

        if (intent.action == OPEN_TASK_DIALOG) {
            val taskTitle: String? = intent.getStringExtra(EXTRA_ITEM_TASK_TITLE)

            val i = Intent(context, TaskDialog::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
            i.putExtra(EXTRA_ITEM_TASK_TITLE, taskTitle)
            context.startActivity(i)
        } // On Click for each task

        if (intent.hasExtra(TASKS_WIDGET_UPDATE)) {
            val ids = intent.extras!!.getIntArray(TASKS_WIDGET_UPDATE)
            onUpdate(context, mgr, ids!!)
        }
        super.onReceive(context, intent)
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        appWidgetIds.forEach { appWidgetId ->
            val views = RemoteViews(context.packageName, R.layout.tasks_layout)
            val intent = Intent(context, TasksRemoteViewsService::class.java).apply {
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
            }
            views.setRemoteAdapter(R.id.list_view_tasks, intent)

            val nowWidgetPendingIntent: PendingIntent = Intent(
                context, TasksWidgetProvider::class.java
            ).run {
                action = OPEN_TASK_DIALOG
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
                PendingIntent.getBroadcast(context, 1, this, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            views.setPendingIntentTemplate(R.id.list_view_tasks, nowWidgetPendingIntent)

            appWidgetManager.updateAppWidget(appWidgetId, views)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.list_view_tasks)
        }
    }

    companion object {
        fun updateWidget(context: Context) {
            val man = AppWidgetManager.getInstance(context)
            val ids = man.getAppWidgetIds(
                ComponentName(context, TasksWidgetProvider::class.java)
            )
            val updateIntent = Intent()
            updateIntent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            updateIntent.putExtra(TASKS_WIDGET_UPDATE, ids)
            context.sendBroadcast(updateIntent)
        }
    }
}