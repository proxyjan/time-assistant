package com.tireswings.tstimeassistant.database

import android.graphics.Color
import android.os.Handler
import android.util.Log
import com.tireswings.tstimeassistant.App
import java.time.Duration
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.math.log

@Singleton
class Repository @Inject constructor(
    private val recordDao: RecordDao,
    private val taskDao: TaskDao
) {

    init {
       /*val entity1 = RecordEntity(Calendar.getInstance().time, "math", Color.WHITE, 0, 0)
       val entity2 = RecordEntity(Calendar.getInstance().time, "geometry", Color.YELLOW, 5, 5)

        val task1 = TaskEntity("math", 1,5, 30, Calendar.getInstance().time, Calendar.getInstance().time, Color.WHITE , Calendar.getInstance().time ,45, isEnabled = true, hasDeadline = true)
        val task2 = TaskEntity("geometry", 4,5, 30, Calendar.getInstance().time, Calendar.getInstance().time, Color.YELLOW, Calendar.getInstance().time ,45, isEnabled = true, hasDeadline = false)

        App.executorService.execute{
            recordDao.insertAll(entity1, entity2)
            taskDao.insertAll(task1, task2)
        }*/
    }


    //fun getRecordsByDate(date: Date): LiveData<List<RecordEntity>> {
    //    return recordDao.findByDate(date)
    //}
    //fun getRecordsByTitle(title: String): LiveData<List<RecordEntity>> {
    //    return recordDao.findByTitle(title)
    //}

    fun insertTaskAsync(resultHandler: Handler, callback: ((List<Long>) -> Unit), task: TaskEntity){
        App.executorService.execute {
            try {
                val response = taskDao.insertAll(task)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while inserting the task")}
            }
        }
    }

    fun insertRecordAsync(resultHandler: Handler, callback: ((List<Long>) -> Unit), record: RecordEntity){
        App.executorService.execute {
            try {
                val response = recordDao.insertAll(record)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while inserting the record")}
            }
        }
    }

    fun getRecordAsync(resultHandler: Handler, callback: ((RecordEntity?) -> Unit), taskTitle: String, date: Date){
        App.executorService.execute {
            try {
                val response = recordDao.findUniqueRecord(taskTitle, date)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while getting the record")}
            }
        }
    }

    fun getTaskAsync(resultHandler: Handler, callback: ((TaskEntity?) -> Unit), taskTitle: String){
        App.executorService.execute {
            try {
                val response = taskDao.findByTitle(taskTitle)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while getting the task")}
            }
        }
    }

    fun getRecordsAsync(resultHandler: Handler, callback: (List<RecordEntity>) -> Unit){
        App.executorService.execute {
            try {
                val response = recordDao.getAll()
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while getting records")}
            }
        }
    }

    //fun getRecordsSync(): List<RecordEntity> {
    //    return recordDao.getAll()
    //}

    fun getTasksAsync(resultHandler: Handler, callback: (List<TaskEntity>) -> Unit){
        App.executorService.execute {
            try {
                val response = taskDao.getAll()
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while getting tasks")}
            }
        }
    }

    fun updateRecordAsync(resultHandler: Handler, callback: (Unit) -> Unit, recordEntity: RecordEntity){
        App.executorService.execute {
            try {
                val response = recordDao.updateRecords(recordEntity)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while updating record")}
            }
        }
    }

    fun updateTaskAsync(resultHandler: Handler, callback: (Unit) -> Unit, taskEntity: TaskEntity){
        App.executorService.execute {
            try {
                val response = taskDao.updateTasks(taskEntity)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while updating task")}
            }
        }
    }

    fun deleteRecordAsync(resultHandler: Handler, callback: (Int) -> Unit, recordEntity: RecordEntity){
        App.executorService.execute {
            try {
                val response = recordDao.delete(recordEntity)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while deleting record")}
            }
        }
    }

    fun deleteTaskAsync(resultHandler: Handler, callback: (Int) -> Unit, taskEntity: TaskEntity){
        App.executorService.execute {
            try {
                val response = taskDao.delete(taskEntity)
                resultHandler.post { callback(response) }
            } catch (e: Exception) {
                resultHandler.post { print("error while deleting task")}
            }
        }
    }

    fun preProcessingDataForGeneratingRecordsAsync(resultHandler: Handler, callback: (GenerateInputData) -> Unit) {
        App.executorService.execute {
            try {
                val tasks = taskDao.getAll().associateBy ({it.title}, {it})
                val records = recordDao.getAll()
                val count = tasks.count()
                val g = GenerateInputData(count)
                val tasksByTitle : MutableMap<String, MutableMap<Int, Int>> = mutableMapOf()
                val today = Date()

                for (record in records) {
                    if(record.taskTitle in tasks.keys) {
                        val task = tasks[record.taskTitle]!!
                        val diffDays = ( (today.time - record.date.time) / (1000 * 60 * 60 * 24) ).toInt()
                        var p = 0
                        if(diffDays > task.unitsPeriod){
                            p = (diffDays / task.unitsPeriod)
                        }
                        if(record.taskTitle !in tasksByTitle) {
                            tasksByTitle[record.taskTitle] = mutableMapOf()
                        }
                        val taskPeriods = tasksByTitle[record.taskTitle]!!
                        if (p in taskPeriods) {
                            val n: Int = taskPeriods[p]!!
                            taskPeriods[p] = n + (record.doneTime)
                        } else {
                            taskPeriods[p] = record.doneTime
                        }
                    }
                }

                for ((index, taskEntity) in tasks.values.withIndex()) {
                    g.index[index] = index
                    g.totalTime[index] = taskEntity.unitsTotal
                    g.totalDoneTime[index] = 0
                    tasksByTitle[taskEntity.title]?.let { t ->
                        g.totalDoneTime[index] = t.values.reduce{ sum, element -> sum + element }
                    }
                    g.totalPosPer[index] = 0
                    if(taskEntity.hasDeadline){
                        val d2 = today.time - taskEntity.createdDate.time
                        val d1 = taskEntity.deadlineDate.time - taskEntity.createdDate.time
                        g.totalPosPer[index] = (d2 * 100 / d1).toInt()
                        val d3 =  d1 / (taskEntity.unitsPeriod * (1000 * 60 * 60 * 24) )
                        g.totalTime[index] = g.totalTime[index] * d3.toInt()
                    }
                    g.period[index] = taskEntity.unitsPeriod
                    g.periodTotalTime[index] = taskEntity.unitsTotal
                    g.periodDoneTime[index] = 0
                    val d = ((today.time - taskEntity.createdDate.time) / (1000 * 60 * 60 * 24) ).toInt()
                    val currentPeriod = d / taskEntity.unitsPeriod
                    tasksByTitle[taskEntity.title]?.let { t ->
                        if(t.containsKey(currentPeriod)) g.periodDoneTime[index] = t[currentPeriod]!!
                    }
                    g.periodPosPer[index] = (( d % taskEntity.unitsPeriod) * 100 / taskEntity.unitsPeriod).toInt()
                    g.unitDuration[index] = taskEntity.unitDuration
                    g.remainingTime[index] = currentPeriod * g.periodTotalTime[index]
                    tasksByTitle[taskEntity.title]?.let{ t->
                        val filteredKeys = t.filterKeys { it < currentPeriod }
                        if(filteredKeys.count() > 0) g.remainingTime[index] -= filteredKeys.values.reduce{ sum, element -> sum + element }
                    }
                    g.userPriority[index] = 1
                }

                resultHandler.post { callback(g) }
            } catch (e: Exception) {
                resultHandler.post { print("...")}
            }
        }
    }

    fun postProcessingDataForGeneratingRecordsAsync(index: IntArray, time: IntArray) {
        App.executorService.execute {
            try {
                val today = App.getToday()
                val tasks = taskDao.getAll()
                for (ind in index) {
                    if(time[ind] <= 0) continue
                    val task = tasks[ind]
                    val record = recordDao.findUniqueRecord(task.title, today)
                    if(record == null){
                        val r = RecordEntity(today, task.title, task.color, 0, time[ind])
                        recordDao.insertAll(r)
                    }
                    else{
                        recordDao.updateRecords(RecordEntity(today, task.title, task.color, record.doneTime, time[ind]))
                    }
                }

                App.fetchRecords()
            } catch (e: Exception){
                print("error doing post processing stuff")
            }
        }
    }
}
