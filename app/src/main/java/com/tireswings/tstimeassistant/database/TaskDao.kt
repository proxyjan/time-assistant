package com.tireswings.tstimeassistant.database

import androidx.room.*

@Dao
interface TaskDao {
    @Query("SELECT * FROM tasks")
    fun getAll(): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE title IN (:titles)")
    fun loadAllByTitles(titles: Array<String>): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE title IS (:title)")
    fun findByTitle(title: String): TaskEntity?

    @Query("UPDATE tasks SET isEnabled = (:isEnabled) WHERE title IS (:title)")
    fun setIsEnabled(title: String, isEnabled: Boolean): Int

    @Transaction
    @Query("SELECT * FROM tasks")
    fun getTasksWithRecords(): List<TaskWithRecords>

    @Insert
    fun insertAll(vararg tasks: TaskEntity): List<Long>

    @Delete
    fun delete(task: TaskEntity): Int

    @Update
    fun updateTasks(vararg tasks: TaskEntity)

}